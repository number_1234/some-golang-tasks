package main

import "fmt"

func main() {
	var n interface{}

	n = 123

	fmt.Printf("n = %+v\n", n)
	switch n.(type) {
	case int:
		fmt.Print("n is int\n")
	case string:
		fmt.Print("n is string\n")
	case bool:
		fmt.Print("n is bool\n")
	default: // не понял, что делать с типом "channel"
		fmt.Print("n is channel\n")
	}
}
