package main

import (
	"fmt"
	"sync"
)

func pow2(val int, vg *sync.WaitGroup) {
	fmt.Println(val * val)
	vg.Done() //
}

func main() {
	var nums = []int{2, 4, 6, 8, 10}
	var wg sync.WaitGroup

	for i := range nums {
		wg.Add(1) // добавл. значение к счетчкику vg
		go pow2(nums[i], &wg)
	}
	wg.Wait() // блок. исп. потока до тех пор, пока счетчик vg не обнулится
}
