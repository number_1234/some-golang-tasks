package main

import (
	"fmt"
	"math/rand"
)

func main() {
	var nums []int
	var i int

	for j := 0; j < 10; j++ {
		nums = append(nums, rand.Intn(20))
	}

	fmt.Printf("%+v\n", nums)
	fmt.Printf("Enter i (0 <= i <= %d):\n", len(nums)-1)
	fmt.Scanf("%d", &i)

	part1 := nums[0:i]
	if i != len(nums)-1 {
		part1 = append(part1, nums[i+1:]...)
	}

	fmt.Printf("Deleting %dth element from slice:\n%+v\n", i, part1)
}
