package main

import (
	"bufio"
	"fmt"
	"os"
	"unicode/utf8"
)

func main() {
	var reversed string
	reader := bufio.NewReader(os.Stdin)

	rawString, _ := reader.ReadBytes('\n')

	for i := utf8.RuneCount(rawString) - 1; i >= 0; i-- {
		reversed += string(rawString[i])
	}
	fmt.Printf("%s\n", reversed)
}
