package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// func valueInArray(key int, arr []int) bool { // чето не так
// 	var left int = -1
// 	var right int = len(arr)

//		for left < right-1 {
//			mid := (left + right) / 2
//			if arr[mid] == key {
//				return true // mid + 1
//			}
//			if arr[mid] < key {
//				left = mid
//			} else {
//				right = mid
//			}
//		}
//		return false // -1
//	}

func valueInArray(key int, arr []int) bool { // ok
	for i := 0; i < len(arr); i++ {
		if arr[i] == key {
			return true
		}
	}
	return false
}

func getSetsIntersection(arr1 []int, arr2 []int) (result []int) {
	for _, num := range arr1 {
		if (valueInArray(num, arr2)) && !(valueInArray(num, result)) {
			result = append(result, num)
		}
	}
	for _, num := range arr2 {
		if (valueInArray(num, arr1)) && !(valueInArray(num, result)) {
			result = append(result, num)
		}
	}
	return
}

func main() {
	var rawInputArr [][]string
	var set_a, set_b []int

	reader := bufio.NewReader(os.Stdin)
	for i := 0; i < 2; i++ {
		rawLine, _ := reader.ReadString('\n')
		lineArr := strings.Fields(rawLine)
		rawInputArr = append(rawInputArr, lineArr)
	}

	for i := range rawInputArr {
		for j := 0; j < len(rawInputArr[i]); j++ {
			num, _ := strconv.Atoi(rawInputArr[i][j])
			switch i {
			case 0:
				set_a = append(set_a, num)
			case 1:
				set_b = append(set_b, num)
			}
		}
	}

	fmt.Printf("%+v\n", getSetsIntersection(set_a, set_b))
}
