package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	var result []string
	reader := bufio.NewReader(os.Stdin)
	rawString, _ := reader.ReadString('\n')
	strArr := strings.Fields(rawString)
	for i := len(strArr) - 1; i >= 0; i-- {
		result = append(result, strArr[i])
	}

	fmt.Printf("%s\n", strings.Join(result[:], " "))
}
