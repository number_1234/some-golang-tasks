package main

import (
	"fmt"
	"sync"
)

func main() {
	var sum int
	var nums = []int{2, 4, 6, 8, 10}
	// для ожидания заверш. всех горутин
	var wg sync.WaitGroup
	wg.Add(len(nums))

	for _, n := range nums {
		// запустим горутину для каждого числа
		go func(x int) {
			defer wg.Done()
			sum += x * x
			fmt.Printf("%d\n", sum)
		}(n)
	}
	// нужно дождаться заверш. всех горутин
	wg.Wait()
	fmt.Printf("сумма квадратов чисел: %v = %d\n", nums, sum)
}
