package main

import (
	"fmt"
	"sync"
)

func main() {
	var data = []string{"apple", "banana", "pear", "peach", "mango"}
	result := make(map[string]int, len(data))
	var wg sync.WaitGroup
	wg.Add(len(data))

	for idx, item := range data {
		go func(key string, val int) {
			defer wg.Done()
			result[key] = val
			fmt.Printf("%+v\n", result)
		}(item, idx)
	}

	wg.Wait()
	fmt.Printf("result:\n%+v\n", result)
}
