package main

import (
	"fmt"
)

func main() {
	var n, i int64

	fmt.Scanf("%d", &n)
	fmt.Scanf("%d", &i)

	fmt.Printf("%b\n", n)
	n = (n >> i) & 1

	fmt.Printf("%d\n", n)
}
