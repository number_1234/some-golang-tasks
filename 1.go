package main

import "fmt"

// type Action struct {
// 	someValue int
// 	isOk      bool
// }

type Action struct {
	Human
}

func (a *Action) walk() {
	fmt.Printf("%s likes watching movies\n", a.name)
	a.sayHello()
}

type Human struct {
	name string
	age  uint
}

func (h *Human) sayHello() {
	fmt.Printf("Hello, my name is %s, my age is %d\n", h.name, h.age)
}

func main() {
	h := Action{
		Human{
			name: "Andrey",
			age:  20,
		},
	}
	h.walk()
}
